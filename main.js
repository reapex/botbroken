const Discord = require('discord.js');
const bot = new Discord.Client();
const fortnite = require('fortnite.js');
const mysql = require('mysql')
const { token, prefix, sqlConfig, fortnite_api_key, color, log_channel, mute_id, kick, ban, stats, link, unlink, warn } = require('./config.json')
var sql = mysql.createConnection({
    host: sqlConfig.host,
    user: sqlConfig.user,
    password: sqlConfig.password,
    database: sqlConfig.database
});
const client = new fortnite(fortnite_api_key);

bot.on('ready', async () => {
    console.log(`En ligne.`);
    sql.connect();
});

async function sendStats(username, msg) {
    const search = await client.get(username, fortnite.PC).catch(err => console.log(err))
    const fullKd = search["stats"].kd
    const fullTop1 = search["stats"].top1
    const soloTop1 = search["solo"].top1.value
    const soloKd = search["solo"].kd.value
    const soloWinrate = search["solo"].winRatio.value
    const kills = search["stats"].kills
    const duoTop1 = search["duo"].top1.value
    const duoKd = search["duo"].kd.value
    const duoWinrate = search["duo"].winRatio.value
    const squadTop1 = search["squad"].top1.value
    const squadKd = search["squad"].kd.value
    const squadWinrate = search["squad"].winRatio.value

    embed = new Discord.MessageEmbed()
        .setColor(color)
        .setTitle(`🕵🏼‍♂️ Stats de ${username}`)
        .addField(`👑 **Top 1 :**`, `${fullTop1}`, true)
        .addField(`📊 **K/D :**`, `${fullKd}`, true)
        .addField(`🔫 **Kills :**`, `${kills}`, true)
        .addField(`👑 **Solo Top 1 :**`, `${soloTop1}`, true)
        .addField(`📊 **Solo K/D :**`, `${soloKd}`, true)
        .addField(`🔫 **Solo winrate :**`, `${soloWinrate}%`, true)
        .addField(`👑 **Duo Top 1 :**`, `${duoTop1}`, true)
        .addField(`📊 **Duo K/D :**`, `${duoKd}`, true)
        .addField(`🔫 **Duo winrate :**`, `${duoWinrate}%`, true)
        .addField(`👑 **Squad Top 1 :**`, `${squadTop1}`, true)
        .addField(`📊 **Squad K/D :**`, `${squadKd}`, true)
        .addField(`🔫 **Squad winrate :**`, `${squadWinrate}%`, true)
    msg.channel.send(embed)
    log_channel_id.send(embed)

}


bot.on('message', async msg => {

    log_channel_id = msg.guild.channels.cache.find(e => e.id === log_channel)

    if (msg.content === prefix + 'stats') {
        if (stats === "false") return;
        msg.delete()

        sql.query(`SELECT epic FROM bk WHERE id_discord = "${msg.author.id}"`, async function (err, result) {
            if (result[0] === undefined) {
                embed = new Discord.MessageEmbed()
                    .setColor(color)
                    .setDescription('Veuillez lier votre compte.')

                return msg.channel.send(embed)
            }
            else {
                sendStats(result[0]['epic'], msg)
            }
        })
    }

    if (msg.content.startsWith(prefix + 'link')) {
        if (link === "false") return;
        msg.delete()

        let username = msg.content.slice(6)
        if (!username) {
            embed = new Discord.MessageEmbed()
                .setColor(color)
                .setDescription('Veuillez saisir un pseudonyme !')

            return msg.channel.send(embed)
        }

        try {
            await client.get(username, fortnite.PC);
        } catch (error) {
            embed = new Discord.MessageEmbed()
                .setColor(color)
                .setDescription('Utilisateur introuvable.')

            return msg.channel.send(embed)
        }


        sql.query(`SELECT id_discord FROM bk WHERE id_discord = "${msg.author.id}"`, function (err, result) {
            if (result.length === 1) {
                embed = new Discord.MessageEmbed()
                    .setColor(color)
                    .setDescription('Votre compte est déjà relié. Utilisez **!unlink** avant de réessayer.')
                return msg.channel.send(embed)
            }
            else {
                sql.query(`INSERT INTO bk(id_discord, epic) VALUES ("${msg.author.id}",  "${username}")`, function (err, result) {
                    if (err) {
                        console.log(err)
                    } else {
                        embed = new Discord.MessageEmbed()
                            .setColor(color)
                            .setDescription(`Vous venez de relier le compte **${username}** !`)
                        return msg.channel.send(embed)
                    }
                });
            }
        })
    }

    if (msg.content === prefix + "unlink") {
        if (unlink === "false") return;
        msg.delete()
        var id_discord = msg.author.id;

        sql.query(`DELETE FROM bk WHERE id_discord = "${id_discord}"`, function (err, result) {
            if (err) {
                console.log(err)
            }
            else {
                embed = new Discord.MessageEmbed()
                    .setColor(color)
                    .setDescription('Votre compte à bien été unlink !')
                return msg.channel.send(embed)
            }
        })
    }

    if (msg.content.startsWith(prefix + 'kick')) {
        if (kick === "false") return;
        let mentionMember = msg.mentions.members.first();
        if (!mentionMember) {
            embed = new Discord.MessageEmbed()
                .setColor(color)
                .setDescription('Saisissez un utilisateur à kick.')
            return msg.channel.send(embed)
        }
        else if (!mentionMember.kickable) {
            embed = new Discord.MessageEmbed()
                .setColor(color)
                .setDescription('Impossible de kick l\'utilisateur')
            return msg.channel.send(embed)
        }
        else {
            try {

                embed = new Discord.MessageEmbed()
                    .setColor(color)
                    .setDescription(`L\'utilisateur ${mentionMember} a bien été kick.`)

                mentionMember.kick()
                    .then(() => msg.channel.send(embed))

                log_channel_id.send(embed)
                
            } catch{
                embed = new Discord.MessageEmbed()
                    .setColor(color)
                    .setDescription('Une erreur est survenue.')
                return msg.channel.send(embed)
            }
        }
    }

    if (msg.content.startsWith(prefix + 'ban')) {
        if (ban === "false") return;
        let mentionMember = msg.mentions.members.first();
        if (!mentionMember) {
            embed = new Discord.MessageEmbed()
                .setColor(color)
                .setDescription('Saisissez un utilisateur à ban.')
            return msg.channel.send(embed)
        }
        else if (!mentionMember.bannable) {
            embed = new Discord.MessageEmbed()
                .setColor(color)
                .setDescription('Impossible de kick l\'utilisateur')
            return msg.channel.send(embed)
        }
        else {
            try {

                embed = new Discord.MessageEmbed()
                    .setColor(color)
                    .setDescription(`L\'utilisateur ${mentionMember} a bien été ban.`)

                mentionMember.ban()
                    .then(() => msg.channel.send(embed))
                log_channel_id.send(embed)

            } catch{
                embed = new Discord.MessageEmbed()
                    .setColor(color)
                    .setDescription('Une erreur est survenue')
                return msg.channel.send(embed)
            }
        }
    }

    if (msg.content.startsWith(prefix + 'warn')) {
        if (warn === "false") return;
        let mentionMember = msg.mentions.members.first();
        let mentionId = mentionMember.id;

        sql.query(`SELECT warn FROM warn WHERE id_discord = "${mentionId}"`, function (err, result) {
            if (result[0] === undefined) {
                sql.query(`INSERT INTO warn(warn, id_discord) VALUES('1', '${mentionId}')`)
            }
            else {
                sql.query(`SELECT warn FROM warn WHERE id_discord = "${mentionId}"`, async function (err, result) {
                    if (err) {
                        console.log(err)
                    } else {

                        if (result[0]['warn'] === 2) {
                            sql.query(`DELETE FROM warn WHERE id_discord = "${mentionId}"`)
                            sql.query(`INSERT INTO warn(warn, id_discord) VALUES('${result[0]['warn'] + 1}', '${mentionId}')`)

                            await mentionMember.roles.add(mute_id)

                            embed = new Discord.MessageEmbed()
                                .setColor(color)
                                .addField(`⚠️Avertissement par ${msg.author.username}`, `<@${mentionId}> vient de recevoir un warn ! (Total ${result[0]['warn'] + 1}).\n=> L'utilisateur a bien été mute.`)

                            msg.channel.send(embed)
                            log_channel_id.send(embed)
                        }
                        else if (result[0]['warn'] === 5) {
                            
                            if (!mentionMember.bannable) {
                                embed = new Discord.MessageEmbed()
                                    .setColor(color)
                                    .setDescription('Impossible de kick l\'utilisateur')
                                return msg.channel.send(embed)
                            }
                            else {
                                try {
                                    await mentionMember.ban().then()
                                    embed = new Discord.MessageEmbed()
                                    .setColor(color)
                                    .setDescription(`L'utilisateur ${mentionMember} a bien été bannis.`)

                                    log_channel_id.send(embed)
                                } catch{
                                    console.log(err)
                                }
                            }

                            sql.query(`DELETE FROM warn WHERE id_discord = "${mentionId}"`)
                            sql.query(`INSERT INTO warn(warn, id_discord) VALUES('${result[0]['warn'] + 1}', '${mentionId}')`)

                            embed = new Discord.MessageEmbed()
                                .setColor(color)
                                .addField(`⚠️Avertissement par ${msg.author.username}`, `<@${mentionId}> vient de recevoir un warn ! (Total ${result[0]['warn'] + 1}).\n=> L'utilisateur a bien été banni !`)

                            msg.channel.send(embed)
                            log_channel_id.send(embed)
                        }
                        else {
                            sql.query(`DELETE FROM warn WHERE id_discord = "${mentionId}"`)
                            sql.query(`INSERT INTO warn(warn, id_discord) VALUES('${result[0]['warn'] + 1}', '${mentionId}')`)

                            embed = new Discord.MessageEmbed()
                                .setColor(color)
                                .addField(`⚠️Avertissement par ${msg.author.username}`, `<@${mentionId}> vient de recevoir un warn ! (Total ${result[0]['warn'] + 1})`)

                            msg.channel.send(embed)
                            log_channel_id.send(embed)
                        }
                    }
                })
            }
        })

    }

})

bot.login(token);