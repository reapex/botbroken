**Installation** :

Pour installer les packages nécessaires :

Utilisez ``npm install`` en ligne de commande ou bien installez les différents packages à la main :

```npm i discord.js```, ```npm i fortnite.js```, ```npm i mysql```

**MySql** :

Créer les bases en éxécutant ses script depuis la base de données :

``
CREATE TABLE `bk` (
	`id_discord` TEXT(65535) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`epic` LONGTEXT NULL DEFAULT NULL COLLATE 'latin1_swedish_ci'
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
;
``

puis :

``CREATE TABLE `warn` (
	`warn` INT(11) NULL DEFAULT NULL,
	`id_discord` TEXT(65535) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci'
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
;
``


**Config** :

Utilisez le fichier ``config.json`` pour configurer le bot, vous pouvez désactiver des commandes !

**Assistance** : 

Discord : Reapex#1313
Twitter : twitter.com/reapexzer